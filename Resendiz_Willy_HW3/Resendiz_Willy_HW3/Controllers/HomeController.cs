﻿//Name: Willy Resendiz
//Date: 10/18/2022
//Description: HW3 - Seeding & Searching

using Microsoft.AspNetCore.Mvc;
using Resendiz_Willy_HW3.Models;
using Resendiz_Willy_HW3.DAL;
using Microsoft.EntityFrameworkCore;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Resendiz_Willy_HW3.Controllers
{


    public class HomeController : Controller
    {
  

        private AppDbContext _context;

        public HomeController(AppDbContext dbContext)
        {
            _context = dbContext;
        }

        public IActionResult Index(string SearchString)
        {
            var query = from r in _context.Repositories select r;
            if (SearchString != null)
            {
            
                query = query.Where(r => r.RepositoryName.Contains(SearchString) || r.Description.Contains(SearchString));

            }

            List<Repository> SelectedRepositories = query.Include(r => r.Language).ToList();

            //Populate the view bag with a count of all repositories
            ViewBag.AllRepositories = _context.Repositories.Count();

            //Populate the view bag with a count of selected repositories
            ViewBag.SelectedRepositories = SelectedRepositories.Count;

            return View(SelectedRepositories.OrderByDescending(r => r.StarCount));
        }

        public IActionResult Details(int? id)
        {
            if (id == null) //RepositoryID not specified
            {
                //user did not specify a RepositoryID – take them to the error view
                return View("Error", new String[] { "RepositoryID not specified - which repository do you want to view ? " });

            }

            //look up the repo in the database based on the id
            // be sure to include the language
            Repository repository = _context.Repositories.Include(b => b.Language).FirstOrDefault(b => b.RepositoryID == id);

            if (repository == null) //No repository with this id exists in the database
            {
                //there is not a repository with this id in the database – show an error view
                return View("Error", new String[] { "Repository not found in database" });
            }
            // if code gets this far, all is well - display the details
            return View(repository);
        }


        public IActionResult DetailedSearch()
        {
            ViewBag.allLanguages = GetAllLanguagesList();

            return View();

        }
        
        private SelectList GetAllLanguagesList()
        {
            List<Language> languageList = _context.Languages.ToList();

            Language SelectNone = new Language() { LanguageID = 0, LanguageName = "All Languages" };
            languageList.Add(SelectNone);

            SelectList languageSelectList = new SelectList(languageList.OrderBy(m => m.LanguageID), "LanguageID", "LanguageName");

            return languageSelectList;
        }

        public IActionResult DisplaySearchResults(SearchViewModel svm)
        {
            var query = from r in _context.Repositories select r;

            if (svm.Title != null && svm.Title != "")
            {
                query = query.Where(r => r.RepositoryName.Contains(svm.Title));

            }

            if(svm.Description != null && svm.Description != "")
            {
                query = query.Where(r => r.Description.Contains(svm.Description));

            }

            if(svm.Category != null)
            {
                query = query.Where(r => r.Category == svm.Category);

            }

            /// 0 means All languages
            if(svm.LanguageID != 0)
            {
                query = query.Where(r => r.Language.LanguageID == svm.LanguageID);

            }

            if(svm.StarCount != null)
            {
                switch (svm.Compare)
                {
                    case Comparison.GreaterThan:
                        query = query.Where(r => r.StarCount >= svm.StarCount);
                        break;

                    case Comparison.LessThan:
                        query = query.Where(r => r.StarCount <= svm.StarCount);
                        break;

                }
            }

            if(svm.UpdatedAfter != null)
            {
                query = query.Where(r => r.LastUpdate >= svm.UpdatedAfter);
            }


            List<Repository> SelectedRepositories = query.Include(r => r.Language).ToList();

            //Populate the view bag with a count of all repositories
            ViewBag.AllRepositories = _context.Repositories.Count();

            //Populate the view bag with a count of selected repositories
            ViewBag.SelectedRepositories = SelectedRepositories.Count;

            return View("Index",SelectedRepositories.OrderByDescending(r => r.StarCount));

        }

    }
        
}
