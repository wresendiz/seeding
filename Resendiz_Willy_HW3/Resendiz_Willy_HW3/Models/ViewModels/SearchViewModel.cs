﻿using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace Resendiz_Willy_HW3.Models
{
    
    public enum Comparison { GreaterThan, LessThan};

    [Keyless]
    public class SearchViewModel
    {

        [Display(Name = "Search by Title:")]
        public String Title { get; set; }

        [Display(Name = "Search by Description:")]
        public String Description { get; set; }

        [Display(Name = "Search by Category:")]
        public Category? Category { get; set; }

        [Display(Name = "Search by Language:")]
        public Int32 LanguageID { get; set; }

        [Display(Name = "Search by Star Count:")]
        [DisplayFormat(DataFormatString = "{0:n}")]
        public Decimal? StarCount { get; set; }

        [Display(Name = "Type of Search:")]
        public Comparison Compare { get; set; }

        [Display(Name = "Search by Last Updated Date:")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        public DateTime? UpdatedAfter { get; set; }


    }
}
